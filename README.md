## Примеры запуска

Для начала работы приложения необходимо установить все необходимые библиотеки. Это можно сделать следующим способом:
* `pip3 install -r requirements.txt`

Теперь, когда все готово, приступим к запуску приложения:

* HTML-файл с указанием максимальной глубины crawling-а:
  ```shell
  python3 main.py -r --path="data/test.html" --type=html --max-depth=2
  ```

* Файл, который содержит набор ссылок и обрабатывается пакетами, также с указанием глубины crawling-а:
  ```shell
  python3 main.py -r --path="data/urls.txt" --type=urls --max-depth=2
  ```

* HTML-файл:
  ```shell
  python3 main.py --path="data/test.html" --type=html
  ```

* URL:
  ```shell
  python3 main.py -r --path="https://pastebin.com/yYeEN43V" --type=url
  ```

Отчет о найденной информации хранится в файле `data/report.txt`. Вот пример такого отчета:

```

https://pastebin.com/i/facebook.png
===============

https://facebook.com/pastebin
===============
IPv6   : 2a03:2880:f113:81:face:b00c:0:25de
IPv4   : 16.4.55.56
IPv4   : 0.19.56.56
IPv4   : 4.4.52.52
IPv4   : 5.4.52.52
IPv4   : 18.4.52.52
IPv4   : 1.0.0.0
IPv4   : 7.0.24.24
IPv4   : 7.0.24.24
IPv4   : 9.0.32.32
IPv4   : 0.12.160.63
PHONE  : 74943819365
PHONE  : 76630279604
PHONE  : 74943819365
PHONE  : 74943819365
PHONE  : 84780620867
PHONE  : 785992724758
PHONE  : 80854490587
PHONE  : 74944528437
PHONE  : 70036627795
PHONE  : 74944528437
PHONE  : 73819494952
PHONE  : 71937995411
PHONE  : 84656839550
PHONE  : 73819494952
PHONE  : 80569079368
PHONE  : 886721511514
PHONE  : 89100715468
PHONE  : 82889295429
PHONE  : 82894202095
PHONE  : 82889295429
PHONE  : 82249708366
PHONE  : 82285908362
PHONE  : 82249708366
PHONE  : 874967343914
PHONE  : 870370840565
PHONE  : 79444365272
PHONE  : 75361676536
PHONE  : 81982703217
PHONE  : 81469722126
PHONE  : 84538990219
PHONE  : 72267715788
PHONE  : 74836400589
PHONE  : 74890000583
PHONE  : 84246091179
PHONE  : 74836400589
PHONE  : 74917436474
PHONE  : 84479158934
PHONE  : 79243762592
PHONE  : 86992015998
PHONE  : 86992104331
PHONE  : 75959897899
PHONE  : 86992015998
PHONE  : 81119622090
PHONE  : 81374933022
PHONE  : 74943819365
PHONE  : 74629272881
PHONE  : 74943819365
PHONE  : 80564432611
PHONE  : 83236363137
PHONE  : 787882282034650
PHONE  : 83236363137
PHONE  : 787882282034650
PHONE  : 80564432611
PHONE  : 80482132619
PHONE  : 80482132619
PHONE  : 80482132953
PHONE  : 84335371225
PHONE  : 80482132953
PHONE  : 84335371225
PHONE  : 80482132619
PHONE  : 80482132953
PHONE  : 84335371225
PHONE  : 80482132953
PHONE  : 84335371225
PHONE  : 82878801857
PHONE  : 82878801857
PHONE  : 82878835191
PHONE  : 73958219649
PHONE  : 82878801857
PHONE  : 82878835191
PHONE  : 73958219649
PHONE  : 82878835191
PHONE  : 73958219649
PHONE  : 74275407159
PHONE  : 74275407159
PHONE  : 74278740492
PHONE  : 74275407159
PHONE  : 74278740492
PHONE  : 74278740492
PHONE  : 72036854775
PHONE  : 83214759826
PHONE  : 83214759826
PHONE  : 779530704021
PHONE  : 76159830277
PHONE  : 72693363922
PHONE  : 76159830277
PHONE  : 72693363922
PHONE  : 76159830277
PHONE  : 72693363922
PHONE  : 74943819365
PHONE  : 74943819365
PHONE  : 83236363137
PHONE  : 83236363137
PHONE  : 787882282034650
PHONE  : 83236363137
PHONE  : 787882282034650
PHONE  : 83236363137
PHONE  : 83236363137
PHONE  : 787882282034650
PHONE  : 80564432611
PHONE  : 74388763083
PHONE  : 76630279604
PHONE  : 85548666699
PHONE  : 85548666699
PHONE  : 785249324341
PHONE  : 785473156501
PHONE  : 85634592364
PHONE  : 81334239213
PHONE  : 80564432611
PHONE  : 80564432611
PHONE  : 80564432611
PHONE  : 85548666699
PHONE  : 785249324341
PHONE  : 85548666699
PHONE  : 785249324341
PHONE  : 785473156501
PHONE  : 85634592364
PHONE  : 81334239213
PHONE  : 80564432611
PHONE  : 76630279604
PHONE  : 76630279604
SOCIAL : https://twitter.com/pastebin

https://pastebin.com
===============
SOCIAL : https://twitter.com/pastebin

https://www.googletagmanager.com/gtag/js
===============

https://pastebin.com/yYeEN43V
===============
IPv6   : 2001:0db8:85a3:0000:0000:8a2e:0370:7334
IPv6   : FE80:0000:0000:0000:0202:B3FF:FE1E:8329
IPv6   : 2001:0db8:85a3:0000:0000:8a2e:0370:7334
IPv6   : FE80:0000:0000:0000:0202:B3FF:FE1E:8329
IPv4   : 192.168.1.1
IPv4   : 192.168.1.1
IPv4   : 192.168.1.1
IPv4   : 255.255.255.0
IPv4   : 192.168.1.1
IPv4   : 255.255.255.0
E-MAIL : hakimov0666@yandex.ru
E-MAIL : buro-perevodov-trans@yandex.ru
E-MAIL : tester-super-puper@gmail.com
E-MAIL : hakimov0666@yandex.ru
E-MAIL : buro-perevodov-trans@yandex.ru
E-MAIL : tester-super-puper@gmail.com
PHONE  : 79045195420
PHONE  : +79045195420
PHONE  : 89045195420
PHONE  : 79045195420
PHONE  : +79045195420
PHONE  : 89045195420
SOCIAL : https://twitter.com/pastebin

https://twitter.com/pastebin
===============
E-MAIL : logo46x38@2x.png
PHONE  : 84615806686
PHONE  : 86190154277
SOCIAL : https://twitter.com/privacy
SOCIAL : https://twitter.com/pastebin
SOCIAL : https://twitter.com/tos

https://facebook.com/security/hsts-pixel.gif
===============

https://pastebin.com/
===============
SOCIAL : https://twitter.com/pastebin

https://www.googletagmanager.com/debug/bootstrap
===============

https://twitter.com/privacy
===============
SOCIAL : https://twitter.com/settings/safety
SOCIAL : https://twitter.com/Twitter
SOCIAL : https://twitter.com
SOCIAL : https://twitter.com/en/privacy
SOCIAL : https://twitter.com/tos
SOCIAL : http://t.co
SOCIAL : https://twitter.com/your_twitter_data
SOCIAL : https://twitter.com/settings/devices
SOCIAL : https://twitter.com/personalization
SOCIAL : https://twitter.com/privacy
SOCIAL : https://twitter.com/settings/notifications
SOCIAL : https://twitter.com/settings/account
SOCIAL : http://t.co/
SOCIAL : https://twitter.com/settings/your_twitter_data
```
