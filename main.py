import logging
import sys

import crawler_util
from itertools import islice

BATCH_SIZE = 1000


def print_help():
    print('Usage:\n'
          '\t - python3 main.py --path=<URL or path to HTML file> --type=<url|html|urls> [--max-depth=<max depth>] [-r]'
          '\n\nArguments in square brackets are optional.')


args = sys.argv[1:]
is_url = False
is_urls = False
recursive = False
max_depth = float('inf')
path = 'https://medium.com/mind-cafe/four-signs-a-person-is-secretly-unhappy-with-their-life-9d60d4147083'

for arg in args:
    if arg.startswith('--path='):
        path = arg[len('--path='):]
    elif arg.startswith('--type='):
        is_url = arg[len('--type='):] == 'url'

        if arg[len('--type='):] == 'urls':
            is_urls = True

        if not is_url and not is_urls and arg[len('--type='):] != 'html':
            print_help()
    elif arg.startswith('--max-depth='):
        try:
            max_depth = int(arg[len('--max-depth='):])
        except ValueError:
            logging.error(f"Expected: integer value for max depth, found: {arg[len('--max-depth='):]}.")
            exit(0)
    elif arg == '-r':
        recursive = True
    else:
        print_help()

crawler = crawler_util.Crawler()
crawler.max_depth = max_depth

if is_url:
    crawler.get_all_internal_links(path, crawler.get_data(path), not recursive)
elif is_urls:
    with open(path, 'r') as f:
        for n_lines in iter(lambda: tuple(islice(f, BATCH_SIZE)), ()):
            for line in n_lines:
                line = line.strip()
                crawler.get_all_internal_links(line, crawler.get_data(line), not recursive)
else:
    try:
        with open(path, 'r') as f:
            crawler.get_all_internal_links(path, f.read(), not recursive, is_from_file=True)
    except FileNotFoundError:
        logging.error(f'File {path} was not found. Please, check it\'s existence and try again.')
        exit(0)

if recursive:
    crawler.bfs()
