import logging
import re
from urllib.error import HTTPError
from urllib.parse import urlparse
from collections import deque
from collections import defaultdict

import requests
from requests import TooManyRedirects, RequestException

social_networks = ['vk.com', 'fb.com', 't.co', 't.me', 'telegram.org', 'ok.ru', 'fb.me', 'instagram.com', 'instagr.com',
                   'instagr.am', "twitter.com", "vk.cc"]


class Crawler:
    def __init__(self):
        self.q = deque()
        self.dist = defaultdict(lambda: -1)
        self.max_depth = float('inf')
        self.last_depth = -1

        with open('data/report.txt', 'w'):
            pass

        self.writer = open('data/report.txt', 'a')

    def write_report(self, lst: list, tp: str):
        if tp == 'LINK':
            self.writer.write('\n')
            self.writer.write(lst[0])
            self.writer.write('\n')
            self.writer.write('=' * 15)
            self.writer.write('\n')
            return

        tp = tp.ljust(7, ' ')

        for item in lst:
            if tp == "IPv6   ":
                try:
                    item = item[0]

                    if len(item) < 5:
                        continue
                except KeyError:
                    pass
            elif tp == "PHONE  ":
                try:
                    item = item[0]
                except KeyError:
                    pass
            self.writer.write(f'{tp}: {item}')
            self.writer.write('\n')

    def report_all_ips(self, data: str):
        ipv6s = re.findall(
            r'(([0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]'
            r'{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|'
            r'([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|'
            r'[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}'
            r'%[0-9a-zA-Z]+|::(ffff(:0{1,4})?:)?((25[0-5]|(2[0-4]|1?[0-9])?[0-9])\.){3}(25[0-5]|'
            r'(2[0-4]|1?[0-9])?[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1?[0-9])?[0-9])\.)'
            r'{3}(25[0-5]|(2[0-4]|1?[0-9])?[0-9]))',
            data)
        self.write_report(ipv6s, "IPv6")

        ipv4s_init = re.findall(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', data)
        ipv4s = []
        for ip in ipv4s_init:
            correct = True

            for elem in ip.split('.'):
                try:
                    if int(elem) < 0 or int(elem) > 255:
                        correct = False
                except ValueError:
                    correct = False

            if correct:
                ipv4s.append(ip)
        self.write_report(ipv4s, "IPv4")

        emails = re.findall(r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)", data)
        self.write_report(emails, "E-MAIL")

        phones = re.findall(r'((\+7|7|8)+[0-9]{10})', data)
        self.write_report(phones, "PHONE")

        links = self.get_all_links(data)
        social_network_links = []
        for link in links:
            if self.get_domain(link) in social_networks:
                social_network_links.append(link)
        self.write_report(social_network_links, "SOCIAL")

    @staticmethod
    def get_data(url: str):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) '
                          'AppleWebKit/527.16 (KHTML, like Gecko) Chrome/81.0.4032.13 Safari/735.12'}
        try:
            response = requests.get(url, headers=headers)
            response.encoding = 'utf-8'
            data = response.text

            return data
        except ConnectionError:
            logging.error('Error occurred with network')
        except HTTPError:
            logging.error('Error occurred while fetching data from API')
        except TooManyRedirects:
            logging.error('Too many redirects')
        except RequestException:
            logging.error('Unknown error occurred')
        except (KeyError, IndexError):
            logging.error('Unexpected HTML tags structure, probably it changed since last reporter update')

    @staticmethod
    def get_all_links(data: str):
        all_links = re.findall(r'(https?://(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s"()<>]{2,}|'
                               r'www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s"()<>]{2,}|https?://(?:www\.|'
                               r'(?!www))[a-zA-Z0-9]+\.[^\s"()<>]{2,}|www\.[a-zA-Z0-9]+\.[^\s"()<>]{2,})', data)
        for i in range(len(all_links)):
            question_index = all_links[i].find('?')
            if question_index != -1:
                all_links[i] = all_links[i][:question_index]

            sharp_index = all_links[i].find('#')
            if sharp_index != -1:
                all_links[i] = all_links[i][:sharp_index]

        return list(set(all_links))

    @staticmethod
    def get_domain(url: str):
        parsed_uri = urlparse(url)
        return '{uri.netloc}'.format(uri=parsed_uri)

    def get_all_internal_links(self, base_url: str, data: str, is_report=False, is_from_file=False):
        if is_report:
            self.report_all_ips(data)

        all_links = Crawler.get_all_links(data)
        domain = ''
        if not is_from_file:
            domain = self.get_domain(base_url)

        for link in all_links:
            if (domain == '' or self.get_domain(link) == domain) and self.dist[link] == -1:
                self.q.append(link)
                self.dist[link] = self.dist[base_url] + 1

                if self.dist[link] > self.last_depth:
                    print('Started crawling at level', self.dist[link])
                    self.last_depth = self.dist[link]

                if self.dist[link] == self.max_depth:
                    exit(0)

    def bfs(self):
        while len(self.q) > 0:
            link = self.q.popleft()

            self.write_report([link], 'LINK')
            self.get_all_internal_links(link, self.get_data(link), True)
